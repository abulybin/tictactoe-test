/**
 * Created by alexboo on 6/13/16.
 */
$(document).ready(function(){

    var map = $('.map');
    var result = $('.result');
    var game;

    var showSteps = function(game) {
        for (j in game.steps) {
            var step = game.steps[j];
            map.find('.cell-' + step.x + '-' + step.y).addClass(step.isUser ? 'user' : 'bot');
        }

        if (game.result.id == 2) {

            map.children().remove();
            $('#modal-win').modal();

            $('#modal-win').find('.btn-primary').click(function(){
                $.ajax({
                    method: "POST",
                    url: "/api/game/finish",
                    data: JSON.stringify({gameId: game.id, username: $('#modal-win').find('input[name="name"]').val()})
                }).done(function(data){
                    if (data.status) {
                        $('#modal-win').modal('hide');
                    }
                });
            });
        }

        if (game.result.id == 3) {
            map.children().remove();
            $('#modal-lose').modal();
        }
    };

    $('#results').click(function(){
        map.addClass('hidden');

        $.ajax({
            method: "GET",
            url: "/api/game/results"
        }).done(function(data){

            var table = result.find('table');;
            if (data.status) {
                for (var i in data.data) {
                    table.append('<tr>' +
                        '<td>' + data.data[i].username + '</td>' +
                        '<td>' + data.data[i].startDate + '</td>' +
                        '<td>' + data.data[i].finishDate + '</td>' +
                    '</tr>');
                }
            }

            result.removeClass('hidden');
        });


    });

    $('#start-game').click(function(){

        result.addClass('hidden');

        map.children().remove();

        $.ajax({
            method: "POST",
            url: "/api/game/start"
        }).done(function(data){
            game = data['data'];
            if (data.status) {

                for (var x = 1; x <= 20; x ++) {
                    var row = $('<div\/>');
                    for(var y = 1; y <= 20; y ++) {
                        var div = $('<div\/>');
                        div
                            .addClass('cell')
                            .addClass('cell-' + x + '-' + y)
                            .attr('data-x', x)
                            .attr('data-y', y);

                        row.append(div);
                    }

                    map.append(row);
                }
                map.removeClass('hidden');

                $('.cell').unbind('click');
                $('.cell').bind('click', function(){
                    $.ajax({
                        method: "POST",
                        url: "/api/game/step",
                        data: JSON.stringify({x: $(this).data('x'), y: $(this).data('y'), gameId: game.id})
                    }).done(function(data){
                        if (data.status) {
                            game = data.data
                            showSteps(game);
                        }
                    });
                });
            }
        });
    });

});