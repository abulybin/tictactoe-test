<?php

namespace App\Reference;


class Constants
{
    const DELETED_YES = 1;
    const DELETED_NO = 0;

    const DEFAULT_ERROR_CODE = 500;

    const RESULT_BEGIN_ID = 1;
    const RESULT_WIN_ID = 2;
    const RESULT_LOSE_ID = 3;

    const VALIDATE_REQUEST = true;

    const STEP_VALUE_USER = "x";
    const STEP_VALUE_BOT = "o";

    const MAX_CELLS_IN_ROW = 20;
}