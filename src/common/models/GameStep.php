<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 11:44 AM
 */

namespace App\Model;


use App\Reference\Constants;

class GameStep extends BaseModel
{
    protected $id;
    protected $game_id;
    protected $x;
    protected $y;
    protected $is_user;
    protected $date;

    public function initialize()
    {
        $this->belongsTo(
            "game_id",
            "App\Model\Game",
            "id", [
            'alias' => 'Game'
        ]);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getGameId()
    {
        return $this->game_id;
    }

    /**
     * @param mixed $game_id
     */
    public function setGameId($game_id)
    {
        $this->game_id = $game_id;
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param mixed $x
     */
    public function setX($x)
    {
        $this->x = $x;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param mixed $y
     */
    public function setY($y)
    {
        $this->y = $y;
    }

    /**
     * @return mixed
     */
    public function getIsUser()
    {
        return $this->is_user;
    }

    /**
     * @param mixed $is_user
     */
    public function setIsUser($is_user)
    {
        $this->is_user = $is_user;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    public function getStepValue()
    {
        return ($this->is_user ? Constants::STEP_VALUE_USER : Constants::STEP_VALUE_BOT);
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    public function beforeValidationOnCreate()
    {
        $this->setDate(date('Y-m-d H:i:s'));
    }


}