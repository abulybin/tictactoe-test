<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 11:42 AM
 */

namespace App\Model;


use App\Reference\Constants;
use App\Response\Game\GameResponse;

/**
 * Class Game
 * @package App\Model
 * @property Result $result;
 * @property GameStep[] $steps;
 * @property GameResponse $response;
 */
class Game extends BaseModel {
    protected $id;
    protected $result_id;
    protected $username;
    protected $start_date;
    protected $finish_date;

    public function initialize()
    {
        $this->belongsTo(
            "result_id",
            "App\Model\Result",
            "id", [
            'alias' => 'Result'
        ]);

        $this->hasMany(
            "id",
            "App\Model\GameStep",
            "game_id",
            [
                'alias' => 'Steps'
            ]
        );

        $this->createResponseBuilder("response", function(){
            return (new GameResponse())->mapping($this);
        });
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getResultId()
    {
        return $this->result_id;
    }

    /**
     * @param mixed $result_id
     */
    public function setResultId($result_id)
    {
        $this->result_id = $result_id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return mixed
     */
    public function getFinishDate()
    {
        return $this->finish_date;
    }

    /**
     * @param mixed $finish_date
     */
    public function setFinishDate($finish_date)
    {
        $this->finish_date = $finish_date;
    }

    public function finishGame()
    {
        $this->setFinishDate(date('Y-m-d H:i:s'));
    }

    public function beforeValidationOnCreate()
    {
        $this->setStartDate(date('Y-m-d H:i:s'));
        $this->setResultId(Constants::RESULT_BEGIN_ID);
    }


}