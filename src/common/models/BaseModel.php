<?php

namespace App\Model;


use App\Library\Util\ObjectUtils;
use App\Response\BaseResponse;
use Phalcon\DI;

abstract class BaseModel extends \Phalcon\Mvc\Model
{
    protected static $_cache = [];
    protected static $_response_builders = [];

    protected $_response_properties = [];
    protected $_exclude_properties = [];

    protected $_cache_relations = [];

    public function setData($data) {
        $this->assign($data, null, array_keys($data));
    }

    /**
     * Create ResponseBuilder
     * @param null $name
     * @param \Closure $builder
     */
    public function createResponseBuilder($name = null, \Closure $builder)
    {
        if (!empty($name)) {
            $class = get_class($this);
            self::$_response_builders[$class][$name] = $builder;
        }
    }

    /**
     * Set properties to response list
     * @param array $properties
     * @param array $excludes
     * @return $this
     */
    public function setResponseProperties(array $properties, $excludes = [])
    {
        $this->_response_properties = [];
        foreach ($properties as $property) {
            $this->addResponseProperty($property);
        }

        foreach ($excludes as $property) {
            $this->addResponseProperty($property, true);
        }

        return $this;
    }

    /**
     * Add property to response list
     * @param $name
     * @param bool|false $exclude
     * @return $this
     */
    public function addResponseProperty($name, $exclude = false)
    {
        if (!$exclude) {
            $this->_response_properties[] = $name;
        } else {
            $this->_exclude_properties[] = $name;
        }

        return $this;
    }

    /**
     * Build  response based on the above properties
     * @return BaseResponse
     */
    public function buildResponse()
    {
        $objectProperties = ObjectUtils::getProperties($this);
        $response = new BaseResponse();
        if (empty($this->_response_properties)) {
            $this->_response_properties = $objectProperties;
        }
        $properties = array_diff($this->_response_properties, $this->_exclude_properties);
        foreach ($properties as $key) {
            if (in_array($key, $objectProperties)) {
                $method = 'get' . ucfirst($key);
                if (method_exists($this, $method)) {
                    $response->setProperty($key, $this->{$method}());
                } else {
                    $response->setProperty($key, $this->{$key});
                }
            }
        }
        return $response;
    }

    /**
     * Override findFirst with session cache
     * @param null $parameters
     * @return BaseModel
     */
    public static function findFirst($parameters = null)
    {
        if (self::isCache()) {
            $key = get_called_class() . self::_createKey($parameters);
            if (!isset(self::$_cache[$key])) {
                if (self::isCacheIdKey($key)) {
                    $result = self::getCache($key);
                    if (empty($result)) {
                        $result = parent::findFirst($parameters);
                        self::addCache($key, $result);
                    }
                } else {
                    $result = parent::findFirst($parameters);
                }

                self::$_cache[$key] = $result;
            }
            return self::$_cache[$key];
        } else {
            return parent::findFirst($parameters);
        }
    }

    /**
     * Override findFirst with session cache
     * @param null $parameters
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function find($parameters = null)
    {
        if (self::isCache()) {
            $key = get_called_class() . self::_createKey($parameters);
            if (!isset(self::$_cache[$key])) {
                self::$_cache[$key] = parent::find($parameters);
            }
            return self::$_cache[$key];
        } else {
            return parent::find($parameters);
        }
    }

    /**
     * Create cache key
     * @param $parameters
     * @return string
     */
    protected static function _createKey($parameters)
    {
        if (is_numeric($parameters)) {
            return self::generateCacheIdKey($parameters);
        } else if (isset($parameters['bind']) && count($parameters['bind']) == 1) {
            $condition = (isset($parameters[0]) ? $parameters[0] : $parameters['conditions']);
            if (stripos($condition, '[id]') !== false) {
                $val = current($parameters['bind']);
                return self::generateCacheIdKey($val);
            }
        }

        $uniqueKey = array();

        if (!empty($parameters)) {
            foreach ($parameters as $key => $value) {
                if (is_scalar($value)) {
                    $uniqueKey[] = $key . ':' . $value;
                } else {
                    if (is_array($value)) {
                        $uniqueKey[] = $key . ':[' . self::_createKey($value) . ']';
                    }
                }
            }
        }

        return join(',', $uniqueKey);
    }

    /**
     * Clear model cache and rebuild it after save
     */
    public function afterSave()
    {
        $this->clearModelCache();
    }

    /**
     * Clear model cache after delete
     */
    public function afterDelete()
    {
        $this->clearModelCache(false);
    }

    /**
     * Clear model cache
     * @param bool|true $rebuildCache
     */
    public function clearModelCache($rebuildCache = true)
    {
        if (!empty($this->id) && self::isCache()) {
            $key = get_class($this) . self::generateCacheIdKey($this->id);
            self::removeCache($key);
            if ($rebuildCache) {
                $this->beforeCaching();
                self::addCache($key, $this);
            }
            $this->clearCacheRelations();
            // remove cache from response builders
            $class = get_class($this);
            if (!empty(self::$_response_builders[$class])) {
                foreach (self::$_response_builders[$class] as $name => $builder) {
                    if (!empty($this->id)) {
                        $cacheKey = self::generateCacheResponseKey($name, $this->id);
                        self::removeCache($cacheKey);
                    }
                }
            }
        }
    }

    /**
     * Magic get call response builder it if exist
     * @param string $property
     * @return mixed|\Phalcon\Mvc\Model|\Phalcon\Mvc\Model\Resultset|\Phalcon\Mvc\Phalcon\Mvc\Model
     */
    public function __get($property)
    {
        $class = get_class($this);
        if (isset(self::$_response_builders[$class][$property])) {
            $builder = self::$_response_builders[$class][$property]->bindTo($this);
            if (!empty($this->id) && self::isCache()) {
                $cacheKey = self::generateCacheResponseKey($property, $this->id);
                $response = $this->getCache($cacheKey);
                if (empty($response)) {
                    $response = $builder();
                    $this->addCache($cacheKey, $response);
                    $this->addCacheToRelations($cacheKey);
                }
                return $response;
            }
            return $builder();
        }

        return parent::__get($property);
    }

    /**
     * Calls before caching model to storage
     */
    protected function beforeCaching()
    {

    }

    /**
     * Clear all relations keys
     */
    protected function clearCacheRelations()
    {
        if (self::isCache()) {
            $relationKey = $this->generateCacheRelationKey();
            $relations = self::getCache($relationKey);
            if (!empty($relations)) {
                foreach ($relations as $cacheKey) {
                    self::removeCache($cacheKey);
                }
            }
        }
    }

    /**
     * Add cache relation for model
     * @param BaseModel $model
     */
    public function addCacheRelation(BaseModel $model)
    {
        if (self::isCache()) {
            $key = $model->generateCacheRelationKey();
            if (!in_array($key, $this->_cache_relations)) {
                $this->_cache_relations[] = $key;
            }
        }
    }

    /**
     * Add cacheKey to cache relations list
     * @param $cacheKey
     */
    protected function addCacheToRelations($cacheKey)
    {
        if (!empty($this->_cache_relations) && self::isCache()) {
            foreach ($this->_cache_relations as $relationKey) {
                $relations = self::getCache($relationKey);
                if (empty($relations)) {
                    $relations = [];
                }
                $relations[] = $cacheKey;
                self::addCache($relationKey, $relations);
            }

            $this->_cache_relations = [];
        }
    }

    /**
     * Generate model cache key
     * @param $id
     * @return string
     */
    protected static function generateCacheIdKey($id)
    {
        return 'Id:' . $id;
    }

    /**
     * Generate response cache key
     * @param $name
     * @param $id
     * @return string
     */
    protected static function generateCacheResponseKey($name, $id)
    {
        return get_called_class() . ':' . $name . 'Id:' . $id;
    }

    /**
     * Generate relation cache key
     * @return null|string
     */
    protected function generateCacheRelationKey()
    {
        if (!empty($this->id)) {
            return get_class($this) . ':Relation' . $this->id;
        }

        return null;
    }

    /**
     * Get value from cache storage by cache key
     * @param $key
     * @return mixed
     */
    protected static function getCache($key)
    {
        if (self::isCache()) {
            return self::cache()->get($key);
        }

        return null;
    }

    /**
     * Add value to cache storage with cache key
     * @param $key
     * @param $value
     */
    protected static function addCache($key, $value)
    {
        if (self::isCache()) {
            self::cache()->save($key, $value, null);
        }
    }

    /**
     * Remove value from cache storage by cache key
     * @param $key
     * @return mixed
     */
    protected static function removeCache($key)
    {
        if (self::isCache()) {
            unset(self::$_cache[$key]);
            return self::cache()->delete($key);
        }
    }

    /**
     * Check that the key is the key object
     * @param $key
     * @return bool
     */
    protected static function isCacheIdKey($key)
    {
        return stripos($key, get_called_class() . self::generateCacheIdKey('')) === 0;
    }

    /**
     * Get cache storage from DI
     * @return mixed
     */
    protected static function cache()
    {
        if (DI::getDefault()->has('cache')) {
            return DI::getDefault()->getCache();
        }

        return null;
    }

    /**
     * Check that cache is enabled
     * @return bool
     */
    protected static function isCache()
    {
        return DI::getDefault()->has('cache');
    }

    public static function clearSessionCache()
    {
        self::$_cache = [];
    }

    /**
     * Gets internal database connection
     *
     * @return \Phalcon\Db\AdapterInterface
     */
    public function getConnection()
    {
        // TODO: Implement getConnection() method.
    }

    /**
     * Assigns values to a model from an array returning a new model
     *
     * @param array $result
     * @param \Phalcon\Mvc\ModelInterface $base
     * @return \Phalcon\Mvc\ModelInterface $result
     */
    public function dumpResult($base, $result)
    {
        // TODO: Implement dumpResult() method.
    }

    /**
     * Returns DependencyInjection connection service
     *
     * @return string
     */
    public function getConnectionService()
    {
        // TODO: Implement getConnectionService() method.
    }

    /**
     * Reset a model instance data
     */
    public function reset()
    {
        // TODO: Implement reset() method.
    }

    /**
     * Forces that a model doesn't need to be checked if exists before store it
     *
     * @param boolean $forceExists
     */
    public function setForceExists($forceExists)
    {
        // TODO: Implement setForceExists() method.
    }
}