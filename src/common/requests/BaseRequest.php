<?php

namespace App\Request;


use App\Library\Helper\ExceptionHelper;
use App\Reference\Constants;
use Phalcon\Validation;
use Phalcon\Validation\Validator;

class BaseRequest
{
    use ExceptionHelper;

    /**
     * @var Validation
     */
    private $validation;

    const COMPARE_LESS = ' < ';
    const COMPARE_MORE = ' > ';
    const COMPARE_EQUAL = ' = ';
    const COMPARE_LIKE = ' LIKE ';
    const COMPARE_IN = ' IN (?) ';

    const DEFAULT_LIMIT = 10;
    const MAX_LIMIT = 100;

    protected $_mapping = [
        /*'startDate' => ['field' => 'create_date', 'compare' => self::COMPARE_MORE]*/
    ];

    public function __construct($data = null, $validate = Constants::VALIDATE_REQUEST)
    {
        $this->setData($data);

        if ($validate) {
            $errors = $this->validate();
            if (!empty($errors)) {
                $this->throwValidationException($errors);
            }
        }
    }

    /**
     * Set data to request
     * @param $data
     */
    public function setData($data)
    {
        if ($data !== null) {
            if (is_array($data)) {
                $params = $data;
            } else {
                $params = get_object_vars($data);
            }
            foreach ($params as $key => $value) {
                if (property_exists($this, $key)) {
                    $this->$key = $value;
                }
            }
            $this->afterDataSet();
        }
    }

    /**
     * Its run before method validate executed
     * Add validators using $this->addValidator method
     * You can check values to add validators here
     */
    protected function initValidators()
    {
    }

    /**
     * Its run after data set on construct
     */
    protected function afterDataSet()
    {
        $this->prepareLimit();

        $properties = get_object_vars($this);

        foreach ($properties as $property => $value) {
            if (is_string($value)) {
                $this->{$property} = trim($value);
            }
        }
    }

    /**
     * Add request validator
     * @param $attribute
     * @param Validator $validator
     */
    protected function addValidator($attribute, Validator $validator)
    {
        if ($this->validation == null) {
            $this->validation = new Validation();
        }
        $this->validation->add($attribute, $validator);
    }

    /**
     * Validate request
     * @return array - errors
     */
    public function validate()
    {
        $this->validation = null;
        $this->initValidators();

        $errors = [];

        if ($this->validation != null && count($this->validation->getValidators()) > 0) {
            $result = $this->validation->validate($this);
            if ($result->count() > 0) {
                foreach ($result as $message) {
                    $errors[] = $message->getMessage();
                }
            }
        }
        return $errors;
    }

    /**
     * Get all data from request
     * @return array
     */
    public function getData()
    {
        $result = [];
        foreach (get_object_vars($this) as $key => $value) {
            $result[\Phalcon\Text::uncamelize($key)] = $value;
        }
        return $result;
    }

    /**
     * Prepare condition for ActiveRecord
     * @param array $conditions
     * @param array $bind
     * @return array
     */
    public function prepareConditions($conditions = [], $bind = [])
    {
        foreach ($this->_mapping as $property => $params) {
            if (isset($this->{$property}) && !empty($this->{$property})) {

                if ($params['compare'] == self::COMPARE_IN) {
                    $params['compare'] = str_replace('?', '{' . $property . ':array}', $params['compare']);
                    $conditions[] = $params['field'] . $params['compare'];
                } else {
                    $conditions[] = $params['field'] . $params['compare'] . ' :' . $property . ':';
                }

                if ($params['compare'] == self::COMPARE_LIKE) {
                    $bind[$property] = '%' . $this->{$property} . '%';
                } else {
                    $bind[$property] = $this->{$property};
                }
            }
        }

        $result = [
            'conditions' => implode(' and ', $conditions),
            'bind' => $bind
        ];

        if (isset($this->limit)) {
            $result['limit'] = $this->limit;
        }

        if (isset($this->offset)) {
            $result['offset'] = $this->offset;
        }

        return $result;
    }

    /**
     * Prepare limit and offset
     */
    private function prepareLimit()
    {
        if (property_exists(get_class($this), 'limit')) {
            if (null !== $this->limit && $this->limit < self::MAX_LIMIT) {
                $this->limit = (int) $this->limit;
            } else {
                $this->limit = self::DEFAULT_LIMIT;
            }
        }

        if (property_exists(get_class($this), 'offset')) {
            $this->offset = (int) $this->offset;
        }
    }
}