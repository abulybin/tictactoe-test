<?php

namespace App\Dao;


use App\Library\Helper\ExceptionHelper;
use Phalcon\Db\Adapter\Pdo;

class BaseDao
{
    use ExceptionHelper;

    /**
     * Starts transaction
     * @return bool
     */
    public function begin()
    {
        return $this->db->begin();
    }

    /**
     * Commit transaction
     * @return bool
     */
    public function commit()
    {
        return $this->db->commit();
    }

    /**
     * Rollback transaction
     * @return bool
     */
    public function rollback()
    {
        return $this->db->rollback();
    }
}