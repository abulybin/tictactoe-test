<?php

namespace App\Service;


use App\Api\Controllers\BaseInjectable;
use App\Library\Helper\ExceptionHelper;
use Phalcon\DI\Injectable;

class BaseService extends Injectable implements BaseInjectable
{
    use ExceptionHelper;
}