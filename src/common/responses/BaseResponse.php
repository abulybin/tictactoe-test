<?php

namespace App\Response;


use Alexboo\AnnotationMapper\Mapper;

class BaseResponse extends Mapper
{
    public function __construct($data = null)
    {
        /**
         * Trim all string by default
         */
        \Alexboo\AnnotationMapper\Cast\String::isTrim(true);

        $this->setData($data);
    }
    /**
     * Set data to response
     * @param $object
     */
    public function setData($object)
    {
        if (is_object($object)) {
            $properties = get_object_vars($object);
        } else {
            $properties = $object;
        }

        if (!empty($properties)) {
            foreach ($properties as $name => $val) {
                $this->setProperty($name, $val);
            }
        }
    }

    /**
     * Set data to property
     * @param $key
     * @param $val
     */
    public function setProperty($key, $val)
    {
        if (!empty($key) && null !== $val) {
            $key = lcfirst(\Phalcon\Text::camelize($key));
            $this->{$key} = $val;
        }
    }

    /**
     * Call before mapping data to response
     */
    public function onBeforeMapping()
    {
        // uncamelize properties before mapping
        foreach ($this->_properties as $property) {
            $property->setMappingProperty(\Phalcon\Text::uncamelize($property->getMappingProperty()));
        }
    }
}