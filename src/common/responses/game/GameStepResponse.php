<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 11:55 AM
 */

namespace App\Response\Game;


use App\Response\BaseResponse;

class GameStepResponse extends BaseResponse {
    /**
     * @Mapped(type="integer")
     */
    public $id;
    /**
     * @Mapped(type="integer")
     */
    public $x;
    /**
     * @Mapped(type="integer")
     */
    public $y;
    /**
     * @Mapped(type="boolean")
     */
    public $isUser;
    /**
     * @Mapped(type="string")
     */
    public $date;
}