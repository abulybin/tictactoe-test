<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 11:54 AM
 */

namespace App\Response\Game;


use App\Response\BaseResponse;

class ResultResponse extends BaseResponse {
    /**
     * @Mapped(type="integer")
     */
    public $id;
    /**
     * @Mapped(type="string")
     */
    public $name;
}