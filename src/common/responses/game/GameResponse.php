<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 11:53 AM
 */

namespace App\Response\Game;


use App\Response\BaseResponse;

class GameResponse extends BaseResponse {
    /**
     * @Mapped(type="integer")
     */
    public $id;
    /**
     * @Mapped(type="App\Response\Game\GameStepResponse[]")
     */
    public $steps;
    /**
     * @Mapped(type="App\Response\Game\ResultResponse")
     */
    public $result;
    /**
     * @Mapped(type="string")
     */
    public $username;
    /**
     * @Mapped(type="string")
     */
    public $startDate;
    /**
     * @Mapped(type="string")
     */
    public $finishDate;
}