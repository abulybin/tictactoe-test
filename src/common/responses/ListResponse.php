<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 02.03.16
 * Time: 16:37
 */

namespace App\Response;


class ListResponse extends BaseResponse
{
    public function __construct($count = null, $rows = null)
    {
        $this->count = (int) $count;
        $this->rows = $rows;
    }

    public $count;
    public $rows;
}