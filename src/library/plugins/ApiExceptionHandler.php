<?php

namespace App\Library\Plugin;

use App\Library\Exception\BaseException;
use Phalcon\DI;
use Phalcon\Events\Event;
use Phalcon\Logger\Adapter\File as Logger;
use Phalcon\Mvc\Dispatcher;

class ApiExceptionHandler extends BasePlugin
{
    /**
     * @param $event Event
     * @param $dispatcher Dispatcher
     * @param $exception \Exception
     * @return bool
     */
    public function beforeException($event, $dispatcher, $exception)
    {
        if ($exception instanceof BaseException) {
            $dispatcher->setReturnedValue($exception);
        } else {
            $this->getDI()->getLogger()->error("{$exception->getMessage()} {$exception->getFile()}:{$exception->getLine()}");
            $dispatcher->forward(['controller' => 'index', 'action' => 'error500']);
        }
        return false;
    }

}