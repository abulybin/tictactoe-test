<?php

namespace App\Library\Plugin;


use App\Api\Service\UserService;
use App\Reference\Constants;
use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Config;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;

class ApiAcl extends BasePlugin
{
    /**
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function getAcl()
    {
        $acl = new \Phalcon\Acl\Adapter\Memory();
        $acl->setDefaultAction(Acl::DENY);

        foreach ($this->config->acl->roles as $roleName => $roleParams) {
            $inherits = (isset($roleParams->inherits)) ? $roleParams->inherits : null;
            $acl->addRole(new \Phalcon\Acl\Role($roleName), $inherits);
        }

        /**
         * @var $actions Config
         */
        foreach ($this->config->acl->resources as $resource => $actions) {
            $resource = strtolower($resource);
            $actions = $actions->toArray();
            $acl->addResource(new \Phalcon\Acl\Resource($resource), array_map(function($action) {
                return strtolower($action);
            }, array_keys($actions)));

            foreach ($actions as $action => $allows) {
                foreach ($allows as $allowRole) {
                    $acl->allow($allowRole, $resource, strtolower($action));
                }
            }
        }

        return $acl;
    }

    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {

        $account = array('roles' => array('Guest'));

        $this->getDI()->set('user', function () {
            return null;
        }, true);

        $controller = strtolower(str_replace('_', '', $dispatcher->getControllerName()));
        $action = strtolower($dispatcher->getActionName());

        $acl = $this->getAcl();

        foreach ($account['roles'] as $singleRole) {
            if ($acl->isAllowed($singleRole, $controller, $action) == Acl::ALLOW) {
                return true;
            }
        }

        $dispatcher->forward([
            'controller' => 'index',
            'action' => 'errorinvalidtoken'
        ]);

        return false;
    }
}