<?php

namespace App\Library\Plugin;

use App\Library\Exception\BaseException;
use App\Reference\Constants;
use Phalcon\Dispatcher;
use Phalcon\Events\Event;

class ApiResponse extends BasePlugin
{
    public function afterExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        $payload = $dispatcher->getReturnedValue();

        $response = [];
        $hasData = false;
        if ($payload instanceof BaseException) {
            $response['status'] = false;
            $response['error'] = $payload->getData();
            $this->response->setStatusCode(Constants::DEFAULT_ERROR_CODE);
        } else {
            $response['status'] = true;
            if ($payload !== null) {
                $response['data'] = $payload;
                $hasData = true;
            }
        }
        $result = json_encode($response);

        if ($hasData) {
            $eTag = md5($result);
            $receivedETag = $this->request->getHeader('HTTP_IF_NONE_MATCH');
            if (!empty($receivedETag) && $eTag == $receivedETag) {
                $this->response->setStatusCode(304, 'Not Modified');
                return;
            } else {
                $this->response->setHeader('ETag', $eTag);
            }
        }

        $this->response->setHeader('Access-Control-Allow-Origin', '*');
        $this->response->setHeader('Access-Control-Allow-Credentials', true);
        $this->response->setHeader('Access-Control-Allow-Method', 'GET, PUT, POST, DELETE, OPTIONS');
        $this->response->setHeader('Access-Control-Max-Age', 1000);
        $this->response->setHeader('Access-Control-Allow-Headers', 'Content-Type, Content-Range, Content-Disposition, Content-Description, x-api-timezone, x-api-token');

        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent($result);
        $this->response->send();
        die;
    }
}