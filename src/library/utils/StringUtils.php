<?php

namespace App\Library\Util;


class StringUtils
{
    /**
     * Clear all not numeric symbols
     * @param $phone
     * @return mixed
     */
    public static function clearPhone($phone)
    {
        return preg_replace('/[^0-9]{1,}/si', '', $phone);
    }
}