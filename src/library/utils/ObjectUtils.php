<?php

namespace App\Library\Util;


class ObjectUtils
{
    /**
     * Get all object properties
     * @param $object
     * @return array
     */
    public static function getProperties($object)
    {
        $properties = [];
        $refObj = new \ReflectionClass($object);
        foreach ($refObj->getProperties() as $property) {
            if ($property->class === $refObj->name) {
                $properties[] = $property->name;
            }
        }
        return $properties;
    }
}