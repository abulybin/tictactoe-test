<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 1:09 PM
 */

namespace App\Library\Bot;


use App\Library\Helper\ExceptionHelper;
use App\Model\Game;

abstract class BaseBot implements BaseBotInterface {

    use ExceptionHelper;

    public function makeStep(Game $game)
    {
        $this->throwException("Not support yet!");
    }
}