<?php
namespace App\Library\Bot;

use App\Model\Game;

/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 1:11 PM
 */
interface BaseBotInterface
{
    public function makeStep(Game $game);
}