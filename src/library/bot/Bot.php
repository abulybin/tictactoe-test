<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 1:08 PM
 */

namespace App\Library\Bot;


use App\Library\Helper\ExceptionHelper;

class Bot {

    use ExceptionHelper;

    /**
     * @param string $difficulty
     * @return BaseBotInterface
     * @throws \App\Library\Exception\BaseException
     */
    public function createBot($difficulty = "simple")
    {
        $botName = "App\Library\Bot\Difficulty\\" . ucfirst($difficulty) . "Bot";
        if (class_exists($botName)) {
            $bot = new $botName();
            if ($bot instanceof BaseBotInterface) {
                return $bot;
            }
        }

        $this->throwException("Указанный вами Бот не поддерживается системой");
    }
}