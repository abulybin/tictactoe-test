<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 1:09 PM
 */

namespace App\Library\Bot\Difficulty;

use App\Api\Request\Game\MakeStepRequest;
use App\Api\Service\GameService;
use App\Library\Bot\BaseBot;
use App\Model\Game;
use App\Model\GameStep;
use App\Reference\Constants;

class SimpleBot extends BaseBot {

    /**
     * @var Game $game
     */
    private $game;
    private $map = [];

    public function makeStep(Game $game)
    {
        $this->game = $game;

        // generate steps map
        if (empty($this->map)) {
            foreach ($this->game->steps as $step) {
                $this->map[$step->getX()][$step->getY()] = $step->getStepValue();
            }
        }

        //unset it because phalcon cached related objects
        unset($game->steps);

        $coords = $this->getStepCoordinates();

        $service = new GameService();

        return $service->makeStep(new MakeStepRequest(['x' => $coords['x'], 'y' => $coords['y'], 'gameId' => $this->game->getId()]), false);
    }

    /**
     * Get coordinates for step
     * @return array|bool
     */
    private function getStepCoordinates()
    {
        /**
         * @var GameStep $lastStep
         */
        $lastStep = GameStep::findFirst([
            'game_id = :gameId:',
            'bind' => [
                'gameId' => $this->game->getId()
            ],
            'order' => 'date DESC'
        ]);

        if ($lastStep) {

            $coords = $this->calculate($lastStep->getX(), $lastStep->getY());

            if ($coords == false) {
                $coords = $this->calculate($lastStep->getX(), $lastStep->getY(), 0, 1);
            }

            if ($coords == false) {
                $coords = $this->calculate($lastStep->getX(), $lastStep->getY(), 1, 1);
            }
        }

        if ($coords == false) {
            $coords = $this->getRandom();
        }

        return $coords;
    }

    /**
     * Calculate coordinates for blocking user step
     * @param $x
     * @param $y
     * @param int $xStep
     * @param int $yStep
     * @return array|bool
     */
    private function calculate($x, $y, $xStep = 1, $yStep = 0)
    {
        $x = $x - (3*$xStep);
        $y = $y - (3*$yStep);

        $weight = 0;
        while (true) {

            $x += $xStep;
            $y += $yStep;

            if (isset($this->map[$x][$y])) {
                if ($this->map[$x][$y] == Constants::STEP_VALUE_USER) {
                    $weight ++;
                } else {
                    $weight = 0;
                }
            }

            if ($weight == 3) {
                $_x = $x+(1*$xStep);
                $_y = $y+(1*$yStep);
                if (!isset($this->map[$_x][$_y])) {
                    return ['x' => $_x, 'y' => $_y];
                }
                $_x = $x-(3*$xStep);
                $_y = $y-(3*$yStep);
                if (!isset($this->map[$_x][$_y])) {
                    return ['x' => $_x, 'y' => $_y];
                }

                return false;
            }

            if ($x > Constants::MAX_CELLS_IN_ROW || $y > Constants::MAX_CELLS_IN_ROW)
                return false;
        }
    }

    /**
     * Get random coordinates for step
     * @return array
     */
    private function getRandom()
    {
        for ($x = 1; $x ++; $x <= Constants::MAX_CELLS_IN_ROW) {
            for ($y = 1; $y ++; $y <= Constants::MAX_CELLS_IN_ROW) {
                if (!isset($this->map[$x][$y])) {
                    return ['x' => $x, 'y' => $y];
                }
            }
        }
    }
}