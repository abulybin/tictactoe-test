<?php

namespace App\Library\Exception;

use ReflectionClass;

class ValidationException extends BaseException
{
    private $messages = [];

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        if (!empty($message)) {
            $this->addMessage($message);
        }
        \Exception::__construct($message, $code, $previous);
    }

    public function addMessage($message)
    {
        $this->messages[] = $message;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function addMessages(array $messages)
    {
        $this->messages = array_merge($this->messages, $messages);
    }

    public function getData()
    {
        $reflect = new ReflectionClass($this);
        $data = array();
        $data['reason'] = $reflect->getShortName();
        if (!empty($this->messages)) {
            $data['messages'] = $this->messages;
        }
        return $data;
    }
}