<?php

namespace App\Library\Exception;


use ReflectionClass;

class BaseException extends \Exception
{
    public function getData()
    {
        $reflect = new ReflectionClass($this);
        $data = array();
        $data['reason'] = $reflect->getShortName();
        if (!empty($this->messages)) {
            $data['messages'] = $this->messages;
        } else {
            $data['messages'] = [$this->getMessage()];
        }
        return $data;
    }
}