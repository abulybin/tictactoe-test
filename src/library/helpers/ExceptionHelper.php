<?php

namespace App\Library\Helper;


use App\Library\Exception\BaseException;
use App\Library\Exception\ModelException;
use App\Library\Exception\ValidationException;
use App\Reference\Constants;

trait ExceptionHelper
{
    /**
     * Throw model exception
     * @param array|\Phalcon\Mvc\Model\MessageInterface $messages
     * @throws ValidationException
     */
    protected function throwModelException(array $messages)
    {
        $exception = new ModelException();
        $exception->addMessages($messages);

        throw $exception;
    }

    /**
     * Throw ApiException with error messages and clean errors
     * @param $message
     * @param int|null $code
     * @throws BaseException
     */
    protected function throwException($message, $code = Constants::DEFAULT_ERROR_CODE)
    {
        throw new BaseException($message, $code);
    }

    /**
     * Throw ApiException with error messages and clean errors
     * @param array $messages
     * @throws ValidationException
     */
    protected function throwValidationException(array $messages)
    {
        $exception = new ValidationException();
        $exception->addMessages($messages);

        throw $exception;
    }
}