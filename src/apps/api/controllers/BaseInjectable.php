<?php

namespace App\Api\Controllers;
use App\Api\Service\GameService;
use App\Model\User;

/**
 * Interface BaseInjectable
 * @package App\api\controllers
 * @property User $user
 * @property \Phalcon\Db\Adapter\Pdo\Mysql $db
 * @property GameService $GameService;
 */
interface BaseInjectable
{

}