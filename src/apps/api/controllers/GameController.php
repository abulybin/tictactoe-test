<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 2:14 PM
 */

namespace App\Api\Controllers;


use App\Api\Request\Game\FinishRequest;
use App\Api\Request\Game\MakeStepRequest;

class GameController extends ControllerBase {

    /**
     * @Post("/api/game/start")
     */
    public function createAction()
    {
        return $this->GameService->createGame();
    }

    /**
     * @Get("/api/game/results")
     */
    public function resultsAction()
    {
        return $this->GameService->getResults();
    }

    /**
     * @Post("/api/game/step")
     */
    public function stepAction()
    {
        return $this->GameService->makeStep(
            new MakeStepRequest(
                $this->request->getJsonRawBody()
            )
        );
    }

    /**
     * @Post("/api/game/finish")
     */
    public function finishAction()
    {
        return $this->GameService->finishGame(
            new FinishRequest(
                $this->request->getJsonRawBody()
            )
        );
    }
}