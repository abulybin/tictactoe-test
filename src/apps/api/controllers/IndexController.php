<?php

namespace App\Api\Controllers;


use App\Library\Exception\InternalErrorException;
use App\Library\Exception\InvalidTokenException;

/**
 * @SWG\Info(title="EMenu API documentation", version="0.1")
 */
class IndexController extends ControllerBase
{
    public function errorInvalidTokenAction()
    {
        throw new InvalidTokenException;
    }

    public function error500Action()
    {
        throw new InternalErrorException;
    }
}