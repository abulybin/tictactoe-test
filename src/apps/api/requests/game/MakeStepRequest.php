<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 12:51 PM
 */

namespace App\Api\Request\Game;


use App\Request\BaseRequest;
use Phalcon\Validation\Validator\Between;
use Phalcon\Validation\Validator\PresenceOf;

class MakeStepRequest extends BaseRequest {

    public $x;
    public $y;
    public $gameId;

    protected function initValidators()
    {
        $this->addValidator('gameId', new PresenceOf([
            'message' => 'Укажиете ID игры'
        ]));

        $this->addValidator('x', new Between([
            'minimum' => 1,
            'maximum' => 20,
            'message' => 'X координата не может быть ментше 1 и больше 20'
        ]));

        $this->addValidator('y', new Between([
            'minimum' => 1,
            'maximum' => 20,
            'message' => 'Y координата не может быть ментше 1 и больше 20'
        ]));
    }
}