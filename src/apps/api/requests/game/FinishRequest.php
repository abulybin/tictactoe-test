<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 2:11 PM
 */

namespace App\Api\Request\Game;


use App\Request\BaseRequest;
use Phalcon\Validation\Validator\PresenceOf;

class FinishRequest extends BaseRequest {
    public $gameId;
    public $username;

    protected function initValidators()
    {
        $this->addValidator('gameId', new PresenceOf([
            'message' => 'Укажиете ID игры'
        ]));

        $this->addValidator('username', new PresenceOf([
            'message' => 'Укажиете Ваше имя'
        ]));
    }
}