<?php
/**
 * Created by PhpStorm.
 * User: alexboo
 * Date: 6/13/16
 * Time: 12:03 PM
 */

namespace App\Api\Service;


use App\Api\Request\Game\FinishRequest;
use App\Api\Request\Game\MakeStepRequest;
use App\Library\Bot\Bot;
use App\Model\Game;
use App\Model\GameStep;
use App\Reference\Constants;
use App\Service\BaseService;

class GameService extends BaseService {

    private $map = [];

    /**
     * Create new game
     * @return \App\Response\Game\GameResponse
     * @throws \App\Library\Exception\ModelException
     */
    public function createGame()
    {
        $game = new Game();

        if ($game->save() == false) {
            $this->throwModelException($game->getMessages());
        }

        return $game->response;
    }

    /**
     * Get list of finished games
     * @return array
     */
    public function getResults()
    {
        $result = [];
        $games = Game::find(['finish_date IS NOT NULL']);

        foreach ($games as $game) {
            $result[] = $game->response;
        }

        return $result;
    }

    /**
     * Make step by user
     * @param MakeStepRequest $request
     * @param bool $user
     * @return mixed|\Phalcon\Mvc\Model|\Phalcon\Mvc\Model\Resultset|\Phalcon\Mvc\Phalcon\Mvc\Model
     * @throws \App\Library\Exception\BaseException
     * @throws \App\Library\Exception\ModelException
     */
    public function makeStep(MakeStepRequest $request, $user  = true)
    {
        /**
         * Check that game is exist or throw exception
         * @
         */
        $game = $this->getGameById($request->gameId);

        $gameStep = GameStep::findFirst([
            'x = :x: and y = :y: and game_id = :gameId:',
            'bind' => ['x' => $request->x, 'y' => $request->y, 'gameId' => $request->gameId]
        ]);

        if ($gameStep) {
            $this->throwException("Данный ход уже был сделан, нельзя повторить указанный ход");
        }

        $gameStep = new GameStep();
        $gameStep->setX($request->x);
        $gameStep->setY($request->y);
        $gameStep->setGameId($request->gameId);
        if ($user) {
            $gameStep->setIsUser(1);
        } else {
            $gameStep->setIsUser(0);
        }

        if ($gameStep->save() == false) {
            $this->throwModelException($gameStep->getMessages());
        }

        if ($user) {
            $bot = (new Bot())->createBot();
            $bot->makeStep($game);
        }

        if ($this->isWin($game)) {
            if ($user) {
                $game->setResultId(Constants::RESULT_WIN_ID);
            } else {
                $game->setResultId(Constants::RESULT_LOSE_ID);
            }

            if ($game->save() == false) {
                $this->throwModelException($game->getMessages());
            }
        }
        //unset it because phalcon cached related objects
        unset($game->steps);
        return $game->response;
    }

    /**
     * Finish game
     * @param FinishRequest $request
     * @throws \App\Library\Exception\ModelException
     */
    public function finishGame(FinishRequest $request)
    {
        /**
         * Check that game is exist or throw exception
         * @var Game $game
         */
        $game = $this->getGameById($request->gameId);
        $game->setUsername($request->username);

        $game->finishGame();

        if ($game->save() == false) {
            $this->throwModelException($game->getMessages());
        }
    }

    /**
     * Get game data by id
     * @param $id
     * @return \App\Model\Game
     * @throws \App\Library\Exception\BaseException
     */
    protected function getGameById($id)
    {
        $game = Game::findFirst($id);
        if ($game == false) {
            $this->throwException("Указанная вами игра не найдена");
        }

        return $game;
    }

    protected function isWin(Game $game)
    {
        unset($game->steps);

        /**
         * @var GameStep $lastStep
         */
        $lastStep = GameStep::findFirst([
            'game_id = :gameId:',
            'bind' => [
                'gameId' => $game->getId()
            ],
            'order' => 'date DESC'
        ]);

        // generate steps map
        if (empty($this->map)) {
            foreach ($game->steps as $step) {
                $this->map[$step->getX()][$step->getY()] = $step->getStepValue();
            }
        }

        if ($lastStep) {

            $win = $this->calculateWin($lastStep->getX(), $lastStep->getY());

            if ($win == false) {
                $win = $this->calculateWin($lastStep->getX(), $lastStep->getY(), 0, 1);
            }

            if ($win == false) {
                $win = $this->calculateWin($lastStep->getX(), $lastStep->getY(), 1, 1);
            }


            return $win;
        }
    }

    private function calculateWin($x, $y, $xStep = 1, $yStep = 0)
    {
        $x = $x - (6*$xStep);
        $y = $y - (6*$yStep);

        $weight = 0;
        while (true) {

            $x += $xStep;
            $y += $yStep;

            if (isset($this->map[$x][$y])) {
                if ($this->map[$x][$y] == Constants::STEP_VALUE_USER) {
                    $weight ++;
                } else {
                    $weight = 0;
                }
            }

            if ($weight == 5) {
               return true;
            }

            if ($x > Constants::MAX_CELLS_IN_ROW || $y > Constants::MAX_CELLS_IN_ROW)
                return false;
        }
    }
}