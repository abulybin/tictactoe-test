<?php

namespace App\Api\Service;


use App\Api\Request\User\UserSignInRequest;
use App\Api\Request\User\UserSignUpRequest;
use App\Model\User;
use App\Model\UserToken;
use App\Reference\Errors;
use App\Service\BaseService;

class UserService extends BaseService
{
    /**
     * Sing up new user
     * @param UserSignUpRequest $request
     * @return \App\Api\Response\User\UserResponse
     * @throws \App\Library\Exception\ModelException
     */
    public function signUp(UserSignUpRequest $request)
    {
        if ($this->user !== null) {
            $this->throwException(Errors::USER_IS_ALREADY_AUTHENTICATED);
        }

        $user = User::findFirstByPhone($request->phone);

        if ($user !== false) {
            $this->throwException(Errors::USER_IS_EXIST);
        }

        $user = new User();
        $user->setData($request->getData());

        if ($user->save() === false) {
            $this->throwModelException($user->getMessages());
        }

        return $user->response;
    }

    /**
     * Sing in for user by phone
     * @param UserSignInRequest $request
     * @return mixed|\Phalcon\Mvc\Model|\Phalcon\Mvc\Model\Resultset|\Phalcon\Mvc\Phalcon\Mvc\Model
     * @throws \App\Library\Exception\BaseException
     * @throws \App\Library\Exception\ModelException
     */
    public function signIn(UserSignInRequest $request)
    {
        if ($this->user !== null) {
            $this->throwException(Errors::USER_IS_ALREADY_AUTHENTICATED);
        }

        $user = User::findFirstByPhone($request->phone);

        if ($user === false) {
            $this->throwException(Errors::WRONG_LOGIN_OR_PASSWORD);
        }

        if ($user->checkPassword($request->password) === false) {
            $this->throwException(Errors::WRONG_LOGIN_OR_PASSWORD);
        }

        $token = new UserToken();
        $token->setUserId($user->getId());
        if ($token->save() === false) {
            $this->throwModelException($token->getMessages());
        }

        return $token->response;
    }

    /**
     * Get user profile
     * @return mixed
     * @throws \App\Library\Exception\BaseException
     */
    public function getProfile()
    {
        if ($this->user !== null) {
            return $this->user->response;
        } else {
            $this->throwException(Errors::USER_IS_NOT_EXIST);
        }
    }

    /**
     * Sign in by token
     * @param string $token
     * @return UserToken
     */
    public function signInByToken($token = null)
    {
        $token = UserToken::findFirstByToken($token);

        if ($token !== false) {
            if ($token->isValid()) {
                return $token;
            }
        }

        return null;
    }
}