<?php

namespace App\Api;

use App\Library\Plugin\ApiAcl;
use App\Library\Plugin\ApiExceptionHandler;
use App\Library\Plugin\ApiResponse;
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\Dispatcher;

class Module implements ModuleDefinitionInterface
{

    /**
     * Registers the module auto-loader
     */
    public function registerAutoloaders(\Phalcon\DiInterface $di = null)
    {
        $di->getLoader()->registerNamespaces(require ROOT_PATH . '/config/namespaces.php', TRUE)->register();
    }

    /**
     * Registers the module-only services
     *
     * @param Phalcon\DI $di
     */
    public function registerServices(\Phalcon\DiInterface $di)
    {

        /**
         * Read configuration
         */
        $config = $di->get('config');
        $config->merge(include __DIR__ . "/config/config.php");
        $config->merge(new \Phalcon\Config\Adapter\Json(APP_PATH . '/api/config/acl.json'));

        /**
         * Setting up the view component
         */
        $di['view'] = function () {
            $view = new View();
            $view->setViewsDir(__DIR__ . '/views/');

            return $view;
        };

        $di['dispatcher'] = function () use ($di) {
            $dispatcher = new Dispatcher();
            $eventsManager = $di->getShared('eventsManager');
            $eventsManager->attach('dispatch', new ApiAcl($di));
            $eventsManager->attach("dispatch:beforeException", new ApiExceptionHandler());
            $eventsManager->attach("dispatch:afterExecuteRoute", new ApiResponse());
            $dispatcher->setEventsManager($eventsManager);
            return $dispatcher;
        };

    }

}
