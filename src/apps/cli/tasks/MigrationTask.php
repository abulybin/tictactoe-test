<?php

class MigrationTask extends BaseTask
{
    /**
     * Generate migration files
     * @param array $params
     */
    public function generateAction(array $params = [])
    {
        $this->exec('generate', $params);
    }

    /**
     * Run migration
     * @param array $params
     */
    public function runAction(array $params = [])
    {
        $this->exec('run', $params);
    }

    /**
     * Show help
     */
    public function mainAction()
    {
        $this->exec();
    }

    /**
     * Executing migration
     * @param string $action
     * @param array $params
     */
    protected function exec($action = '', array $params = [])
    {

        if (!empty($action)) {
            $commands = $this->commands($params);

            if ($action == 'generate') {
                $config = $this->config->migration;
                if (!empty($config->version)) {
                    $commands .= " --version={$config->version}";
                }
            }
            $commands = "./vendor/phalcon/devtools/phalcon.php migration {$action} {$commands}";
        } else {
            $commands = "./vendor/phalcon/devtools/phalcon.php migration";
        }

        $output = [];
        exec($commands, $output);

        echo implode(PHP_EOL, $output) . PHP_EOL;
    }

    /**
     * Generate commands line
     * @param array $params
     * @return string
     */
    protected function commands(array $params = [])
    {
        $config = $this->config->migration;
        $commands = "--migrations={$config->path} --force --no-auto-increment --config=config/db.php";
        if (!empty($params)) {
            $commands .= ' ' . implode(' ', $params);
        }
        return $commands;
    }
}