<?php

use Phalcon\DI\FactoryDefault\CLI as CliDI,
    Phalcon\CLI\Console as ConsoleApp,
    Phalcon\Mvc\View;

$di = new CliDI();

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__) . '/../../'));

defined('APP_PATH') || define('APP_PATH', realpath(ROOT_PATH . '/apps'));

defined('LOGS_PATH') || define('LOGS_PATH', realpath(ROOT_PATH . '/../logs'));

error_reporting(E_ALL);

require ROOT_PATH . '/vendor/autoload.php';

require_once ROOT_PATH . '/config/services.php';

$loader->registerNamespaces(require ROOT_PATH . '/config/namespaces.php', TRUE);
$loader->registerDirs([APP_PATH . '/cli/tasks'], TRUE);
$loader->register();

$di->remove('logger');

$console = new ConsoleApp();
$console->setDI($di);

$arguments = [];
$params = [];

foreach($argv as $k => $arg) {
    if($k == 1) {
        $arguments['task'] = $arg;
    } elseif($k == 2) {
        $arguments['action'] = $arg;
    } elseif($k >= 3) {
        $params[] = $arg;
    }
}
if(count($params) > 0) {
    $arguments['params'] = $params;
}

define('CURRENT_TASK', (isset($argv[1]) ? $argv[1] : null));
define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));

try {
    $console->handle($arguments);
}
catch (\Phalcon\Exception $e) {
    echo $e->getMessage();
    exit(255);
}