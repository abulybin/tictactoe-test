<?php

/**
 * Services are globally registered in this file
 */

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\DI\FactoryDefault;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Security;

$config = include ROOT_PATH . "/config/config.php";
$config->merge(include ROOT_PATH . "/config/db.php");

$di['config'] = $config;

$loader = new \Phalcon\Loader();

$loader->registerNamespaces(require_once ROOT_PATH . '/config/namespaces.php', TRUE)->register();

$di->set('loader', function() use ($loader) {
    return $loader;
}, true);

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di['url'] = function () {
    $url = new UrlResolver();
    $url->setBaseUri('/app/');

    return $url;
};

/**
 * Start the session the first time some component request the session service
 */
$di['session'] = function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
};

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di['db'] = function () use ($config) {
    return new DbAdapter($config->database->toArray());
};

$di->set('logger', function() use ($config) {
    return new FileLogger(LOGS_PATH . "/".date('d-m-Y').'.log');
}, true);

$di->set('security', function () {

    $security = new Security();

    // Set the password hashing factor to 12 rounds
    $security->setWorkFactor(12);

    return $security;
}, true);

$services = require_once ROOT_PATH . '/config/di.php';

foreach ($services as $name => $service) {
    $di->set($name, $service, TRUE);
}
