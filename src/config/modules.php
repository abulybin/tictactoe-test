<?php

/**
 * Register application modules
 */
$application->registerModules([
    'frontend' => [
        'className' => 'App\Frontend\Module',
        'path' => APP_PATH . '/frontend/Module.php'
    ],
    'api' => [
        'className' => 'App\Api\Module',
        'path' => APP_PATH . '/api/Module.php'
    ]
]);