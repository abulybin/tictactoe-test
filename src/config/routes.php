<?php

$di->set('router', function () {
    $router = new \Phalcon\Mvc\Router\Annotations(false);
    $router->removeExtraSlashes(true);

    $router->addModuleResource('frontend', 'App\Frontend\Controllers\Index');
    $router->addModuleResource('api', 'App\Api\Controllers\Index');
    $router->addModuleResource('api', 'App\Api\Controllers\Game');

    $router->setDefaultModule('frontend');
    $router->setDefaultController('App\Frontend\Controllers\Index');
    $router->setDefaultAction('index');

    $router->notFound([
        'module' => 'frontend',
        "controller" => "App\Frontend\Controllers\Index",
        "action"     => "index"
    ]);

    return $router;
});