<?php

return [
    // api module
    'App\Api\Controllers' => APP_PATH . '/api/controllers/',
    'App\Api\Request' => APP_PATH . '/api/requests/',
    'App\Api\Request\Game' => APP_PATH . '/api/requests/game',
    'App\Api\Service' => APP_PATH . '/api/services/',

    // frontend module
    'App\Frontend' => APP_PATH . '/frontend/',
    'App\Frontend\Controllers' => APP_PATH . '/frontend/controllers/',
    'App\Frontend\Request' => APP_PATH . '/frontend/requests/',
    'App\Frontend\Response' => APP_PATH . '/frontend/responses/',
    'App\Frontend\Service' => APP_PATH . '/frontend/services/',

    // common files
    'App\Dao' => ROOT_PATH . '/common/dao/',
    'App\Model' => ROOT_PATH . '/common/models/',
    'App\Service' => ROOT_PATH . '/common/services/',
    'App\Request' => ROOT_PATH . '/common/requests/',
    'App\Response' => ROOT_PATH . '/common/responses/',
    'App\Response\Game' => ROOT_PATH . '/common/responses/game',
    'App\Reference' => ROOT_PATH . '/common/reference/',

    // library files
    'App\Library\Exception' => ROOT_PATH . '/library/exceptions/',
    'App\Library\Plugin' => ROOT_PATH . '/library/plugins/',
    'App\Library\Util' => ROOT_PATH . '/library/utils/',
    'App\Library\Helper' => ROOT_PATH . '/library/helpers/',
    'App\Library\Bot' => ROOT_PATH . '/library/bot/',
    'App\Library\Bot\Difficulty' => ROOT_PATH . '/library/bot/difficulty',
];