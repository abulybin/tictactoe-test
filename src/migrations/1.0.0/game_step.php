<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class GameStepMigration_100
 */
class GameStepMigration_100 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('game_step', array(
                'columns' => array(
                    new Column(
                        'id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'game_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'size' => 11,
                            'after' => 'id'
                        )
                    ),
                    new Column(
                        'x',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'size' => 4,
                            'after' => 'game_id'
                        )
                    ),
                    new Column(
                        'y',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'size' => 4,
                            'after' => 'x'
                        )
                    ),
                    new Column(
                        'is_user',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'size' => 1,
                            'after' => 'y'
                        )
                    ),
                    new Column(
                        'date',
                        array(
                            'type' => Column::TYPE_DATETIME,
                            'size' => 1,
                            'after' => 'is_user'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('id'), 'PRIMARY'),
                    new Index('FK_game_step_game', array('game_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'FK_game_step_game',
                        array(
                            'referencedSchema' => 'tictactoe',
                            'referencedTable' => 'game',
                            'columns' => array('game_id'),
                            'referencedColumns' => array('id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

    /**
     * This method is called after the table was created
     *
     * @return void
     */
     public function afterCreateTable()
     {
        $this->batchInsert('game_step', array(
                'id',
                'game_id',
                'x',
                'y',
                'is_user',
                'date'
            )
        );
     }
}
