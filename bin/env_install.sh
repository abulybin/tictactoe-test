#!/usr/bin/env bash
# install envirenment for pleaple web service for debian/ubuntu linux start by root

apt-get update
echo "Install Redis\n"
apt-get install redis-server

echo "Install Mysql \n"
apt-get install mysql-server

echo "Install php"
apt-get install php5-cli php5-fpm php5-mysql php5-redis php5-gd php5-curl php5-json php5-imagick php5-dev

echo "Install Nginx\n"
apt-get install nginx

echo "Install git\n"
apt-get install git

echo "Install phalcon\n"
apt-get install git-core gcc autoconf
apt-get install libpcre3-dev
cd /tmp
git clone --depth=1 git://github.com/phalcon/cphalcon.git
cd cphalcon/build
./install
cd ../../
rm -rf cphalcon
echo extension=phalcon.so | tee /etc/php5/mods-available/phalcon.ini
php5enmod phalcon

echo "Install SPL_Types for PHP"
pecl install SPL_Types
echo extension=spl_types.so | tee /etc/php5/mods-available/spl_types.ini
php5enmod spl_types

service php5-fpm restart

echo "Install composer\n"
cd /tmp
php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
php -r "if (hash('SHA384', file_get_contents('composer-setup.php')) === 'fd26ce67e3b237fffd5e5544b45b0d92c41a4afe3e3f778e942e43ce6be197b9cdc7c251dcde6e2a52297ea269370680') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); }"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer
chmod +x /usr/local/bin/composer

echo "Environment is installed\n"